*=======================================================*
*	[A]tari [G]ame [T]ools / dml 2016
*=======================================================*
*	Blitter register defs
*-------------------------------------------------------*

BLiTSXI			=	$ffff8a20		; 2
BLiTSYI			=	$ffff8a22		; 2
BLiTSRC			=	$ffff8a24		; 4
BLiTEM1			=	$ffff8a28		; 2
BLiTEM2			=	$ffff8a2a		; 2
BLiTEM3			=	$ffff8a2c		; 2
BLiTDXI			=	$ffff8a2e		; 2
BLiTDYI			=	$ffff8a30		; 2
BLiTDST			=	$ffff8a32		; 4
BLiTXC			=	$ffff8a36		; 2
BLiTYC			=	$ffff8a38		; 2
BLiTHOP			=	$ffff8a3a		; 1
BLiTLOP			=	$ffff8a3b		; 1
BLiTCTRL		=	$ffff8a3c		; 1
BLiTSKEW		=	$ffff8a3d		; 1
